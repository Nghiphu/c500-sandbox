# Copyright (c) 2015 Vikas Iyengar, iyengar.vikas@gmail.com (http://garage4hackers.com)
# Copyright (c) 2016 Detux Sandbox, http://detux.org
# See the file 'COPYING' for copying permission.

import pexpect
import paramiko
import time
from ConfigParser import ConfigParser
from hashlib import sha256
from magic import Magic
import os
import random
import telnetlib
from multiprocessing import Process
import socket

class Sandbox:
    def __init__(self, config_path):
        self.config = ConfigParser()
        self.config.read(config_path)
        self.default_cpu = self.config.get("detux", "default_cpu")

    def execute(self, binary_filepath, platform, sandbox_id,runtime, interpreter = None):
        sandbox_starttime = time.time()
        sandbox_endtime   = sandbox_starttime
        vm_exec_time = self.config.getint("detux", "vm_exec_time")
        #qemu_command = self.qemu_commands(platform, sandbox_id) 
        pcap_folder = self.config.get("detux", "pcap_folder")
        if not os.path.isdir(pcap_folder):
            os.mkdir(pcap_folder)
        host = self.config.get(platform+"-"+sandbox_id, "ip")
        user = self.config.get(platform+"-"+sandbox_id, "user")
        serverip=self.config.get(platform+"-"+sandbox_id, "web_server_ip")
        #macaddr  = self.config.get(platform+"-"+sandbox_id, "macaddr")
        password = self.config.get(platform+"-"+sandbox_id, "password")
        #port  = self.config.getint(platform+"-"+sandbox_id, "port")
        pcap_command = "sudo /usr/bin/dumpcap -i %s -P -w %s -f 'not ((tcp dst port %d and ip dst host %s) or (tcp src port %d and ip src host %s))'"
        # A randomly generated sandbox filename       
        dst_binary_filepath = "Sample/" + ("".join(chr(random.choice(xrange(97,123))) for _ in range(random.choice(range(6,12)))))
        sha256hash = sha256(open(binary_filepath, "rb").read()).hexdigest()
        interpreter_path = { "python" : "/usr/bin/python", "perl" : "/usr/bin/perl", "sh" : "/bin/sh", "bash" : "/bin/bash"  }
        #if qemu_command == None :
        #    return {}
        #qemu_command += " -net nic,macaddr=%s -net tap -monitor stdio" % (macaddr,)  
        #print qemu_command   
        #qemu = pexpect.spawn(qemu_command)
        try: 	
            print("chao cac ban")
            qemu_command="telnet localhost 1234"
            print(qemu_command)   
            qemu = pexpect.spawn(qemu_command)
            qemu.expect("(qemu).*")
            qemu.sendline("info network")
            qemu.expect("(qemu).*")
            qemu.sendline("loadvm init")
            #qemu.kill()
            qemu.sendcontrol(']')
            qemu.expect("telnet.*")
            qemu.sendline("quit")
            print("quit")
            #check open ssh or telnet 
            if self.checkOpen(host,23):
		    	#Copy file to VM by scp 
                (head, filename) = os.path.split(binary_filepath)
                cmd="cp %s /var/www/html/Malware" % binary_filepath
                os.system(cmd)
	            # if have telnet 
                self.telnet_execute(host, user, password,"wget  http://%s:8080/Malware/%s"%(serverip,filename))
                print(serverip)
                print "[+] Binary transferred"
                # Pre binary execution commands
		    	# Grant execute privilege to binary file.
                pre_exec  = self.telnet_execute(host, user, password, "chmod a+x %s" % (filename,))
                pre_exec =  self.telnet_execute(host, user, password,"cat %s"%(filename))
	            # Start Packet Capture
                #pcap_filepath = os.path.join(pcap_folder, "%s_%d.cap" %(sha256hash,time.time(),))
                #pcapture = pexpect.spawn(pcap_command % (ifname, pcap_filepath, 23, host, 23, host))
                #print pcap_command % (ifname, pcap_filepath, 23, host, 23, host)
                #print "[+] Packet Capture started"
	            # Wait for pcapture to start and then Execute the binary
                time.sleep(5)
	            #Tao ra ten file de ghi file strace (ben trong thu muc log)
                (head, filename) = os.path.split(binary_filepath)
	           #Chay lenh strace tren file da duoc truyen vao trong may ao.
	           #Ghi du lieu strace vao thu muc stracelog ben trong may ao sau do telnet de lay du lieu ma may that
                command_to_exec="./%s" % (filename,)
                print "[+] Executing %s" % (command_to_exec,)
                exec_telnet = self.telnet_execute(host, user, password, command_to_exec)
                time.sleep(float(runtime))
            # if use SSH
            elif self.checkOpen(host,22):
                (head, filename) = os.path.split(binary_filepath)
                cmd="cp %s /var/www/html/Malware" % binary_filepath
                os.system(cmd)
                self.ssh_execute(host,user,password,"wget http://%s:8080/Malware/%s"%(serverip,filename))
                print(serverip)
                print "[+] Binary transferred"
                # Pre binary execution commands
                # Grant execute privilege to binary file.
                pre_exec  = self.ssh_execute(host, user, password, "chmod a+x %s" % (filename,))
                pre_exec =  self.ssh_execute(host, user, password,"cat %s"%(filename))
                # Start Packet Capture
                #pcap_filepath = os.path.join(pcap_folder, "%s_%d.cap" %(sha256hash,time.time(),))
                #pcapture = pexpect.spawn(pcap_command % (ifname, pcap_filepath, 22, host, 22, host))
                #print pcap_command % (ifname, pcap_filepath, 22, host, 22, host)
                #print "[+] Packet Capture started"
                # Wait for pcapture to start and then Execute the binary
                time.sleep(5)
	            #Tao ra ten file de ghi file strace (ben trong thu muc log)
                #(head, filename) = os.path.split(binary_filepath)
               #Chay lenh strace tren file da duoc truyen vao trong may ao.
	           #Ghi du lieu strace vao thu muc stracelog ben trong may ao sau do telnet de lay du lieu ma may that
                command_to_exec="./%s" % (filename,)
                print "[+] Executing %s" % (command_to_exec,)
                exec_ssh = self.ssh_execute(host, user, password, command_to_exec)
                time.sleep(float(runtime))
            else:
                print "SSH and telnet does not support!"
        except Exception as e:
            print "[-] Error:", e
            #if qemu.isalive():
            #    qemu.close()
            return {}
        result='abc'
        return result

        
    def identify_platform(self, filepath):
        filemagic = Magic()
        filetype = ""
        try:
            filetype = filemagic.id_filename(filepath)
        except Exception as e:
            # certain version of libmagic throws error while parsing file, the CPU information is however included in the error in somecases
            filetype = str(e)
#        filemagic.close()
        if "ELF 32-bit" in filetype: 
            if "ARM" in filetype:
                return "ELF", "arm"
            if "80386" in filetype:
                return "ELF", "x86"
            if ("MIPS" in filetype) and ("MSB" in filetype):
                return "ELF", "mips"
            if "MIPS" in filetype:
                return "ELF", "mipsel"
            if "PowerPC" in filetype:
                return "ELF", "powerpc"
        if "ELF 64-bit" in filetype:
            if "x86-64" in filetype:
                return "ELF", "x86-64"

        return filetype, self.default_cpu

    def qemu_commands(self, platform, sandbox_id):
        if platform == "x86":
            return "sudo qemu-system-i386 -hda qemu/x86/%s/debian_wheezy_i386_standard.qcow2 -vnc 127.0.0.1:1%s" % (sandbox_id, sandbox_id, )
        if platform == "x86-64":
            return "sudo qemu-system-x86_64 -hda qemu/x86-64/%s/debian_wheezy_amd64_standard.qcow2 -vnc 127.0.0.1:2%s" % (sandbox_id, sandbox_id,)
        if platform == "mips":
            return 'sudo qemu-system-mips -M malta -kernel qemu/mips/%s/vmlinux-3.2.0-4-4kc-malta -hda qemu/mips/%s/debian_wheezy_mips_standard.qcow2 -append "root=/dev/sda1 console=tty0" -vnc 127.0.0.1:3%s'  % (sandbox_id, sandbox_id, sandbox_id,)
        if platform == "mipsel":
            return 'sudo qemu-system-mipsel -M malta -kernel qemu/mipsel/%s/vmlinux-3.2.0-4-4kc-malta -hda qemu/mipsel/%s/debian_wheezy_mipsel_standard.qcow2 -append "root=/dev/sda1 console=tty0" -vnc 127.0.0.1:4%s'  % (sandbox_id, sandbox_id, sandbox_id, )
        if platform == "arm":
            return 'sudo qemu-system-arm -M versatilepb -kernel qemu/arm/%s/vmlinuz-3.2.0-4-versatile -initrd qemu/arm/%s/initrd.img-3.2.0-4-versatile -hda qemu/arm/%s/debian_wheezy_armel_standard.qcow2 -append "root=/dev/sda1" -vnc 127.0.0.1:5%s'  % (sandbox_id, sandbox_id, sandbox_id, sandbox_id,)
        return None


    def checkOpen(self, ip, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ok=False
        try:
            s.connect((ip, int(port)))
            s.shutdown(2)
            ok=True
        except:
            ok=False
        return ok

    def telnet_execute(self, host, user, password, commands, noprompt = False, logout = True):
        tn = telnetlib.Telnet(host)
        tn.read_until("login: ")
        tn.write(user + "\n")
        tn.read_until("Password: ")
        tn.write(password + "\n")
        tn.write(commands + "\n")
        tn.write("exit\n")
        tn.read_all()
        return 0        

    def ssh_execute(self, host, user, password, commands, noprompt = False, logout = True):
        result = None
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(host, port=22, username=user, password=password)
            stdin, stdout, stderr  = ssh.exec_command(commands, timeout=10)
            if logout:
                ssh.close()
            else:
                return ssh # Return SSH object to logout later
        except Exception as e:
            print "[+] Error in ssh_execute: %s" % (e,)
        return result


         
