This site is as an appendix to the paper “A Novel Framework to Classsify Malware in MIPS Architecture-based IoT Devices” submitted to Security and Communication Networks

A novel sandbox able to self-configure the suitable requirement for a target MIPS execu- table file to reveal all behaviors. 
It's based on Firmadyne.

# Install all requirement 
python 2.7 qemu pcaputils sudo libcap2-bin bridge-utils busybox-static fakeroot git dmsetup kpartx netcat-openbsd nmap python-psycopg2 python3-psycopg2 snmp uml-utilities util-linux vlan python-paramiko python-magic python-pexpect

#Install all requirement of Firmadyne

https://github.com/firmadyne/firmadyne

git clone --recursive https://github.com/firmadyne/firmadyne.git

cd firmadyne

./download.sh

cd ../

#Install all requirement of Detux

https://github.com/detuxsandbox/detux


#Clone C500-Sandbox from gitlab


#Clone C500-detux from gitlab

# Merge all item in folder "firmadyne" and "C500-Sandbox" to a folder
# Download a firmware and extract file system using firmadyne, inferNetwork using script inferNetwork.sh of firmdyne

#After that, replace kernel in "binaries/" folder (vmlinux.mipseb,vmlinux.mipsel) by C500-Instrumented kernel (Because C500-Instrumented kernel can't infer Network), keep firmdyne kernel for the next time run inferNetwork.sh, replace C500-Instrumented kernel by firmadyne kernel every time you run inferNetwork.sh and reverse after finishing infer network.

#Convert raw image to qcow2 image using qemu

qemu-img convert -f raw -O qcow2 scratch/1/image.raw scratch/1/image.qcow2
(1 is ID of firmware was assign by firmadyne)

#Replace the final command of scratch/1/run.sh to run QEMU by:
${QEMU} -m 256 -M ${QEMU_MACHINE} -kernel ${KERNEL} \
    -hda image.qcow2 -append "root=${QEMU_ROOTFS} console=ttyS0 nandsim.parts=64,64,64,64,64,64,64,64,64,64 rdinit=/firmadyne/preInit.sh rw debug ignore_loglevel print-fatal-signals=1 user_debug=31 firmadyne.syscall=255" \
    -nographic \
    -net nic,vlan=0 -net tap,vlan=0,id=net0,ifname=${TAPDEV_0},script=no -net nic,vlan=1 -net socket,vlan=1,listen=:2001 -net nic,vlan=2 -net socket,vlan=2,listen=:2002 -net nic,vlan=3 -net socket,vlan=3,listen=:2003 -monitor telnet:127.0.0.1:1234,server,nowait| tee ${WORK_DIR}/qemu.final.serial.log

# Install Inetsimin host machine, run this command in host machine
sudo iptables -t nat -A PREROUTING -i tap1_0 -j redirect

# Run script run.sh to run virtual machine, wait for VM start completely
# Set up gateway, dns-server of virtual machine is ip of nat interface of host machine

# Connect to QEMU monitor via telnet and save a vm snapshot
telnet localhost 1234
(qemu) savevm init 
(qemu) q

# Install apache web server and create a folder name "Malware" in /var/www/html/, change port listen of web server to 8080

#Config parameter in detux.conf, example 
#Config for MIPS Linux VM
[mips-1]
ip=192.168.0.100
user=admin
password=password
macaddr=
port=
web_server_ip=192.168.0.99

# ip is ip of virtual machine
# web_server_ip is ip that install apache web server
# user, password is authentication info 
# macaddr and port does not need config
 
# After config complete, run a sample by following step:
# 1, Run script scratch/1/run.sh
cd scratch/1/
sudo ./run.sh

#2, With each malware sample, run following command:
sudo python detux.py --sample example_binary1 --cpu mips --runtime 5

optional arguments:
  -h, --help            show this help message and exit
  --sample SAMPLE       Sample filenpath (default: None)
  --cpu {x86,x86-64,arm,mips,mipsel}
                        CPU type (default: auto)
  --int {python,perl,sh,bash}
                        Architecture type (default: None)
  --runtime RUNTIME     Time out run binary file (default: 5)

